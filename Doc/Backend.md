# バックエンドの環境構築
バックエンドは`Docker`に乗っているので`docker-compose`で立ち上げる。

## backendに移動
`cd backend`

## パッケージを入れる(初回のみ)
`npm i`

## ルートディレクトリに移動する
`cd ..`

## Dockerのセットアップ
`docker-compose build`

## バックエンドの実行
`docker-compose up`