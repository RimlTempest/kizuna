# フロントエンド環境構築

## frontendのディレクトリに移動する
`cd frontend`  
  
## パッケージを入れる(初回のみ)
`npm i`

## .envファイルの作成

frontend直下に`.env`というファイルを作成する
作成後`.env`ファイルの中に

```dotenv
BASE_URL=http://localhost:3000
SKYWAY_API_KEY=Skywayから持ってきた値
```

を記述する。

## デバック実行
`npm run dev`

## デバック終了
`ctrl + c`を押す