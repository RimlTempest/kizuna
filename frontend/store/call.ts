import {
  Mutation,
  // Action,
  VuexModule,
  getModule,
  Module
} from 'vuex-module-decorators';
import store from '@/store/store';
import { CallState } from '@/types/store/CallType';

@Module({ dynamic: true, store, name: 'call', namespaced: true })
class Call extends VuexModule implements CallState {
  // state
  word: string = '';

  // カウンタに値をセットする mutation
  @Mutation
  public setWord(word: string): void {
    this.word = word;
  }

  @Mutation
  public setReset() {
    this.word = '';
  }

  // stateに向けての値の処理
  // @Action({})
  // public test() {
  //   this.setWord('Sample');
  // }
}

// モジュール化
export const CallModule = getModule(Call);
