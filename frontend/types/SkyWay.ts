import Peer, { SfuRoom } from 'skyway-js';

export type SkywayMediaStream = MediaStream & {
  peerId: string;
};
export type SkywayData = {
  peer: Peer | null;
  room: SfuRoom | null;
  localStream: MediaStream | undefined;
  isVideo: boolean;
  isMute: boolean;
  remoteStreams: SkywayMediaStream[];
  talkingId: string | null;
};
