import { CallState } from '@/types/store/CallType';

export type State = {
  Call: CallState;
};
